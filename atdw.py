import pymongo
import requests
import multiprocessing
from indexer import Indexer


def get_all_products(base_url, key, page, jump):
    client = pymongo.MongoClient()
    db = client.get_database('tfd-development')
    session = requests.Session()
    page = page
    while True:
        resp = session.get(f'{base_url}products?key={key}&out=json&pge={page}&size=100')
        print(resp.status_code)
        if resp.status_code == 200:
            data = resp.json()
            products = data['products']
            for product in products:
                resp = session.get(f"{base_url}product?key={key}&productId={product['productId']}&out=json")
                data = resp.json()
                db.products.insert_one(data)
            if len(products) == 0:
                break
            else:
                page += jump


class ATDWPull():

    def __init__(self):
        client = pymongo.MongoClient()
        db = client.get_database('tfd-development')
        db.products.delete_many({})
        self.key = "d799cc86a6584aef9653069ab8c0f04f"
        self.base_url = 'https://atlas.atdw-online.com.au/api/atlas/'
        self.get_data(no_procs)

    def get_data(self, processes):
        page=1
        jump = processes
        procs = []
        for x in range(processes):
            procs.append(multiprocessing.Process(target=get_all_products, args=(self.base_url, self.key, page, jump)))
            page+=1
            procs[-1].start()
        for proc in procs:
            proc.join()


if __name__ == '__main__':
    no_procs = 4
    puller = ATDWPull()