from pymongo import MongoClient
import csv

def get_vinyards():
    client = MongoClient()
    db = client.get_database('tfd-development')
    recs = db.product_index.find({})
    prod_ids=[]
    for rec in recs:
        if 'winery' in rec['categories']:
            prod_ids.append(rec['product_id'])

    products = []
    for rec in prod_ids:
        products.append(db.products.find_one({'_id':rec}))

    for rec in products:
        print(rec)
        out_rec = {'product_id': rec['_id'],
                   'name': rec['productName'],
                   'productNumber': rec['productNumber'],
                   'address': "{},{},{},{}".format(rec['addresses'][0]['addressLine1'],
                                                   rec['addresses'][0]['addressLine2'],
                                                   rec['addresses'][0]['cityName'],
                                                   rec['addresses'][0]['stateName'],
                                                   rec['addresses'][0]['addressPostalCode']),
                   'lng': rec['addresses'][0]['geocodeGdaLongitude'],
                   'lat': rec['addresses'][0]['geocodeGdaLatitude'],
                   'rating': 3,
                   'cellarStyle': 'Architectural and modern'}
        db.wineries.insert_one(out_rec)


if __name__ == '__main__':
    get_vinyards()