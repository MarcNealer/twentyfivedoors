# TwentyTiveDoors

*****************

### Introduction
This suite of scripts are used to gather records from ATDW, create aset of new categories for each product and then create a search index  based on matching the callar_style with the added categories along with filtering by distance from the winery

## Python Virtual ENV

All python code uses one or more addon libraries. Rather than installing on your whole system
python has a virtual environment system. 

To use these scripts

Create the virtual env using

```shell script
python3 -m venv env
```

Once created activate using

```shell script
. /env/bin/activate
```

Then install the required python libraries using

```shell script
pip install -r requirements.txt
```

The install only needs to be done on a freshly created virtual env. Once all one. you can activate
the virtual env whenever you need to use the scripts


## Pulling data from ATWD

The code for this is in atdw.py. This has on line 29 the API key you might need to update for the future.
other than that, you just need to run this code and it will pull ALL products and services
from ATWD and store them in Mongo. It is multitasking so you will see multiple calls being
made at the same time

RUN Command
```shell script
python atwd.py
```


## Adding Categories
```indexer.py``` scans products and creates the product_index collection in mongo
by default it adds categories for Wineries, Accomodation, Events and Tours, but
you can add a set of custom categories based on key word matches in to the product Description.
In descriptors.json, it has a set of keys with a list of values. They Keys in this are
the categories the product will be assigned to and the values are a list of different
keywords to search for to add to this category.

Example descriptors.json
```json
{
  "Rustic" : ["rustic", "Rustic"],
  "Traditional": ["Traditional", "traditional", "Old fashioned"],
  "Alternative": ["Alternative"],
  "Boutique": ["Boutique"],
  "Cabernet": ["Cabernet"],
  "Riesling": ["riseling"],
  "Chardonnay": ["Chardonnay"],
  "Pinot noir" : ["Pinot noir"],
  "Semillion" : ["Semillon"],
  "Shiraz" : ["Shiraz"],
  "Merlot": ["Merlot"],
  "Sparkling Wine": ["Sparkling Wine"],
  "Relaxing": ["Relaxing", "Tranquil", "tranquility"],
  "Modern": ["Modern"],
  "Cheese": ["Cheese"],
  "Organic": ["Organic"],
  "Family Owned": ["Family Owned"]
}
```

If you want you can add or remove categories from the system using this
json file.

RUN Command

```shell script
python indexer.py
```

## Search Index

Matching a Cellar to a set of products will take too long to do on th fly, so there is a script
to create a search index table. This scans the wineries, and the products and finds products in the
same region as the winery. It them uses a scoring method to determine if the said product is of interest
to the customer looking at the winery.

cellar_scores.json is a json file that lists the cellar styles and defines scores based on the categories
the product is assigned to.

Example cellar_scores.json

```json
{
  "Architectural and modern": {"Alternative": 5, "Modern":  7},
  "Exciting with lots of action": {"Modern": 10},
  "Small and rustic" : {"Rustic": 10, "Boutique": 10},
  "Steeped in history and tradition": {"Family Owned": 4, "Traditional":  10}
}
```
By default the search index adds scores for other wineries, wine tours, accomodation and events,
but all other categories are only scored if they are in the categories linked to the cellar style here

In search_index.py, you can also adjust the search_index defaults

```python
search_distance = 500 # distance of an event/product can be from the winery
min_score = 6 # minimum score a product must obtain to be included in the list for the winery
winery_score = 10  # score to add if a winery
wine_tour_score = 5 # score to add if a wine Tour
accommodation_score = 2 # score to add if Accommodation
event_score = 2 # score to add if an event
```
Items such as acceptable distance from the winery, minimum scores and scores added for the defaults like
wineries and tours are set here

Run command

```shell script
python search_index.py
```

## Using the Search Index.

Since the index is in Mongo, you just need to do a call to search_index
in mongo where search_index.find({'winery':winery['_id'})


