from pymongo import MongoClient
import json


class Indexer:

    def __init__(self):
        client = MongoClient()
        with open("descriptors.json", "r") as f:
            self.desciptor_list = json.load(f)
        self.db = client.get_database('tfd-development')
        self.db.product_index.delete_many({})
        for self.index, self.data in self.__build_new_index__():
            self.__vineyard_index__()
            self.__accomodation__()
            self.__events__()
            self.__wine_tour__()
            self.__clean_index__()
            self.__descriptors__()
            self.__write_out__()

    def __build_new_index__(self):
        new_index = {}
        data = []
        counter = 0
        for rec in self.db.products.find({}):
            if 'productName' in rec.keys():
                new_index[rec['_id']] = set()
                data.append(rec)
                counter += 1
                if counter == 1000:
                    yield new_index, data
                    new_index = {}
                    data = []
                    counter = 0
        yield new_index, data

    def __clean_index__(self):
        new_index = {}
        for key, data in self.index.items():
            if len(data) > 0:
                new_index[key] = data
        self.index = new_index
        new_data = []
        for rec in self.data:
            if rec['_id'] in new_index:
                new_data.append(rec)
        self.data = new_data
        return

    def __write_out__(self):
        for key, data in self.index.items():
            self.db.product_index.insert_one({'product_id': key, 'categories': list(data)})

    def __vineyard_index__(self):
        for rec in self.data:
            if 'vineyard' in rec['productName'].lower() and \
                    'hotel' not in rec['productName'].lower() and \
                    'motel' not in rec['productName'].lower():
                self.index[rec['_id']].add('winery')
            if 'winery' in rec['productName'].lower() and \
                    'hotel' not in rec['productName'].lower() and \
                    'motel' not in rec['productName'].lower() and \
                    'tour' not in rec['productName'].lower():
                self.index[rec['_id']].add('winery')
            if 'estate' in rec['productName'].lower() and 'wine' in rec['productDescription'].lower() and \
                    'hotel' not in rec['productName'].lower() and \
                    'motel' not in rec['productName'].lower() and \
                    'tour' not in rec['productName'].lower():
                self.index[rec['_id']].add('winery')

    def __wine_tour__(self):
        for rec in self.data:
            if 'TOUR' in rec['productCategoryId'] and ('wine' in rec['productDescription'] or
                                                       'vineyard' in rec['productDescription'] or
                                                       'winery' in rec['productDescription']):
                self.index[rec['_id']].add('Wine Tour')

    def __accomodation__(self):
        for rec in self.data:
            if 'ACCOMM' in rec['productCategoryId']:
                if 'wine region' in rec['productDescription'] or 'vineyard' in rec['productDescription'] or \
                        'winery' in rec['productDescription']:
                    self.index[rec['_id']].add('Accommodation')

    def __events__(self):
        for rec in self.data:
            if 'EVENT' in rec['productCategoryId'] and \
                ('wine' in rec['productName'] or
                 'winery' in rec['productName'] or
                 'winery' in rec['productDescription'] or
                 'vineyard' in rec['productDescription']):
                self.index[rec['_id']].add('Event')

    def __descriptors__(self):
        for rec in self.data:
            for cat, descs in self.desciptor_list.items():
                for desc in descs:
                    if desc.lower() in rec['productDescription']:
                        self.index[rec['_id']].add(cat)


if __name__ == '__main__':
    indexer = Indexer()
