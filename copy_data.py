import pymongo

def copy_data():
    client1 = pymongo.MongoClient('ds036079.mlab.com:36079', username='dbuser2', password='Nm@nco12s',
                                  authSource='tfd-development', retrywrites=False)
    client2 = pymongo.MongoClient()

    db_int = client1.get_database('tfd-development')
    db_out = client2.get_database('tfd-development')

    # wineries

    for wr in db_int.wineries.find({}):
        db_out.wineries.insert_one(wr)

    for wine in db_int.wines.find({}):
        db_out.wines.insert_one(wine)

    for region in db_int.regions.find({}):
        db_out.regions.insert_one(region)

if __name__ == '__main__':
    copy_data()