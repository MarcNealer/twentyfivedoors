from pymongo import MongoClient
import requests
import datetime


class AtwdUpdate():
    def __init__(self):
        client = MongoClient()
        self.db = client.get_database('tfd-development')
        self.db.products.create_index('atdwExpiryDate')
        self.db.products.create_index('productId')
        self.session = requests.Session()
        self.key = "d799cc86a6584aef9653069ab8c0f04f"
        self.base_url = 'https://atlas.atdw-online.com.au/api/atlas/'
        self.test_date = datetime.datetime.now() - datetime.timedelta(days=6)
        self.remove_old_products()
        self.get_new_products()

    def remove_old_products(self):
        today = datetime.datetime.now()
        for product in self.db.products.find({}):
            try:
                exp_date = datetime.datetime.strptime(product['atdwExpiryDate'],'%Y-%m-%d')
                if exp_date < today:
                    self.db.products.delete_one({'_id': product['_id']})
            except Exception as e:
                self.db.products.delete_one({'_id': product['_id']})
                print(e)
                print(product)

    def get_new_products(self):
        session = requests.Session()
        page = 1
        while True:
            print(page)
            resp = self.session.get(f'{self.base_url}products?key={self.key}&out=json&pge={page}&size=100')
            data = resp.json()
            products = data['products']
            for product in products:
                try:
                    update_date = datetime.datetime.strptime(product['productUpdateDate'][:10], "%Y-%m-%d")
                    if update_date > self.test_date:
                        if self.db.products.count_documents({'productId':product['productId']}) > 0:
                            self.db.products.delete_many({'productId':product['productId']})
                        resp = session.get(f"{self.base_url}product?key={self.key}&productId={product['productId']}&out=json")
                        data = resp.json()
                        self.db.products.insert_one(data)
                except Exception as e:
                    print(e)
                    print(product)

            if len(products) == 0:
                break
            else:
                page += 1

if __name__ == '__main__':
    AtwdUpdate()