from pymongo import MongoClient
from geopy import distance
import json

search_distance = 500 # distance of an event/product can be from the winery
min_score = 6 # minimum score a product must obtain to be included in the list for the winery
winery_score = 10  # score to add if a winery
wine_tour_score = 5 # score to add if a wine Tour
accommodation_score = 2 # score to add if Accommodation
event_score = 2 # score to add if an event

class SearchIndex():

    def __init__(self):
        with open('cellar_scores.json', 'r') as f:
            self.cellar_scores = json.load(f)
        self.client = MongoClient(username='admin', password='Nm@nco12s')
        self.db = self.client.get_database('tfd-development')
        self.db.search_index.delete_many({})
        self.index_products()

    def index_products(self):
        wineries = self.db.wineries.find({})
        for winery in wineries:
            print(winery)
            rec = {
                "winery": winery["_id"],
                "lat": winery["lat"],
                "lng": winery["lng"],
                "products":[]
            }
            products = self.db.products.find({})
            for product in products:
                if 'addresses' in product:
                    for address in product['addresses']:
                        if address['geocodeGdaLatitude'] and address['geocodeGdaLongitude']:
                              if -90 <= float(address['geocodeGdaLatitude']) <= 90:
                                dis = distance.distance((rec['lat'], rec['lng']),
                                                        (address['geocodeGdaLatitude'], address['geocodeGdaLongitude'])).km
                                if dis < search_distance:
                                    score = self.__get_score__(winery, product)
                                    if score > min_score:
                                        rec['products'].append({"product":product['_id'],
                                                                'score': score,
                                                                "productName": product["productName"],
                                                                "productDescription": product["productDescription"],
                                                                "distance": dis},)
            self.db.search_index.insert_one(rec)

    def __get_score__(self, winery, product):
        cellar_type = winery['cellarStyle']
        if not cellar_type in self.cellar_scores.keys():
            return 0
        else:
            score = 0
            rec = self.db.product_index.find_one({"product_id": product["_id"]})
            if rec:
                cats = rec['categories']
                cellar_scores = self.cellar_scores[cellar_type]
                for tag, tag_score in cellar_scores.items():
                    if tag in cats:
                        score += tag_score
                if 'Winery' in cats:
                    score += winery_score
                if 'Wine Tour' in cats:
                    score += wine_tour_score
                if 'Accommodation' in cats:
                    score += accommodation_score
                if 'Event' in cats:
                    score += event_score
        return score


if __name__ == '__main__':
    indexer = SearchIndex()